function sleep(ms){
  const date = Date.now();
  let currentDate = null;
  do{
    currentDate = Date.now();
  }while(currentDate - date < ms);
}

function quitarAcento(str){
  for(var i=0; i<str.length; i++){
    if(str.charAt(i)=="á") str= str.replace(/á/,"a");
    if(str.charAt(i)=="à") str = str.replace(/à/,"a");

    if(str.charAt(i)=="é") str = str.replace(/é/,"e");
    if(str.charAt(i)=="è") str = str.replace(/è/,"e");

    if(str.charAt(i)=="í") str= str.replace(/í/,"i");
    if(str.charAt(i)=="ì") str = str.replace(/ì/,"i");

    if(str.charAt(i)=="ó") str = str.replace(/ó/,"o");
    if(str.charAt(i)=="ò") str = str.replace(/ò/,"o");

    if(str.charAt(i)=="ú") str = str.replace(/ú/,"u");
    if(str.charAt(i)=="ù") str = str.replace(/ù/,"u");
  }
  return str;
}

//Traducción de mensajes por defecto del JQuery-Validate
$(document).ready(function() {
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  $.extend($.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida",
    pattern: "No estándar",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número entero válido.",
    digits: "Por favor, escribe sólo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    accept: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
  });

  //Validación del Registro con JQuery-Validate
$(document).ready(function(){
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": 0,
    "extendedTimeOut": 0,
    "showEasing": "swing",
    "showMethod": "fadeIn",
    "tapToDismiss": true
  }
  $('#formRegistro').validate({
    validClass: "is-valid",
    errorElement: "div",
    errorClass: "is-invalid",
    onkeyup: function(element){$(element).valid()},
    success: function(label){
      label.html("Válido");
    },
    errorPlacement: function(label, element){
      label.insertAfter(element);
      label.addClass('invalid-feedback');
    },
    highlight: function(label){
      $(label).removeClass("is-valid").addClass("is-invalid");
      $(label).parent().find('div.is-invalid').removeClass("valid-feedback").addClass("invalid-feedback");
    },
    unhighlight: function(label){
      $(label).removeClass("is-invalid").addClass("is-valid");
      $(label).parent().find('div.is-invalid').removeClass("invalid-feedback").addClass("valid-feedback");
    },
    rules: {
      nombre: {
        required: true,
        minlength: 3,
		    namelatam: true,
		    pattern: false
      },

      apellido: {
        required: true,
        minlength: 3,
		    namelatam: true,
		    pattern: false
      },

      mail: {
        utpEmail: true,
        emailExist: true
      },

      contraseña: {
        required: true,
        minlength: 8,
        maxlength: 16,
        checkPattern: true
      },

      valcontraseña: {
        required: true,
        equalTo: '#validationServerContraseña'
      },


    },
    messages: {
      nombre: {
        required: "Por favor introduzca un Nombre",
        minlength: jQuery.validator.format("No se permiten nombres con menos de {0} caracteres"),
		    namelatam: "El campo nombre no debe llevar números ni caracteres especiales"
      },
      apellido: {
        required: "Por favor introduzca un Apellido",
        minlength: jQuery.validator.format("No se permiten apellidos con menos de {0} caracteres"),
		    namelatam: "El campo apellido no debe llevar números ni caracteres especiales"
      },
      mail: {
        required: "Debe introducir un email",
        emailExist: "Ya existe el correo introducido"
      },
      contraseña: {
        required: "La contraseña es obligatoria",
        minlength: jQuery.validator.format("La contraseña debe tener por lo menos {0} caracteres"),
        maxlength: jQuery.validator.format("Te excediste, el límite de caracteres es {0}"),
        checkPattern: "Su contraseña debe contener por lo menos: 1 minúscula, 1 número y 1 mayúscula, se permiten caracteres especiales, pero no espacios"
      },
      valcontraseña: {
        required: "Debe validar su contraseña",
        equalTo: "Sus contraseñas deben coincidir"
      }
    }
  });

  $('#validationServerEmail').focus(function(){
    var nombre = $('#validationServerNombre').val();
    var apellido = $('#validationServerApellido').val();
    nombre = nombre.toLowerCase();

    apellido = apellido.toLowerCase();

    if(nombre && apellido && !this.value){
      this.value = quitarAcento(nombre).trim() + "." + quitarAcento(apellido.trim()) + "@utp.ac.pa";
    }
  });

  $('#validationServerNacimientoDia').bind("change propertychange click keyup blur focus", function(){
    $('#formRegistro').validate().element('#validationServerNacimientoDia');
  });

  $('#validationServerNacimientoMes').bind("change propertychange click keyup blur focus", function(){
    $('#formRegistro').validate().element('#validationServerNacimientoMes');
  });

  $('#validationServerNacimientoAño').bind("change propertychange click keyup blur focus", function(){
    $('#formRegistro').validate().element('#validationServerNacimientoAño');
  });

  $('#validationServerFacultad').bind("change propertychange click keyup blur focus", function(){
    $('#formRegistro').validate().element('#validationServerFacultad');
  });

  $('#validationServerRol').bind("change propertychange click keyup blur focus", function(){
    $('#formRegistro').validate().element('#validationServerRol');
  });

    jQuery.validator.addMethod("utpEmail", function(value, element){
    return this.optional(element) || /^.+@utp.ac.pa$/.test(value);
    }, "Solo se permiten correos de la Universidad Tecnológica de Panamá");

    jQuery.validator.addMethod("namelatam", function(value, element){
      return this.optional(element) || /^[a-zA-ZÁÉÍÓÚñáéíóúÑ ]+$/.test(value);
      });

    jQuery.validator.addMethod("emailExist", function(value, element){
      $('#loader').html('<img class="img-logo" src="Views/Layouts/imgs/loader2.gif" alt="">').fadeOut(1000);
      var dataString = 'email='+value;
      var isSuccess = false;
      $.ajax({
        type: "POST",
        url: "Models/Ajax/ValExistMail.php",
        dataType: "json",
        data: dataString,
        async: false,
        success: function(data){
          $('#loader').fadeIn(1000).html(data); 
          if(data.status == "error"){
            isSuccess = false;
          }
          else{
            isSuccess = true;
          }
        }
      });
      return isSuccess;
    });

    jQuery.validator.addMethod("checkPattern",
      function(value, element) {
            return /^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040]*)(?=.*[A-Z])(?=.*[a-z])\S{8,16}$/.test(value)
    });

    $('#btnLogin').on('click', function(event) {
      $('#loader').html('<img class="img-logo" src="Views/Layouts/imgs/loader2.gif" alt="">').fadeOut(2000);
      event.preventDefault();
      event.stopPropagation();
      var username = $('#InputEmail').val();
      var pass = $('#InputPass').val();
      if((username == "" || username == " ") && (pass == "" || pass == " ")){
        $('#InputEmail').removeClass('is-valid');
        $('#InputEmail').addClass('is-invalid');
        if(!($('#divInpEmail > .invalid-feedback').length)) $('#divInpEmail').append('<div class="invalid-feedback">Porfavor introduzca su correo</div>');

        $('#InputPass').removeClass('is-valid');
        $('#InputPass').addClass('is-invalid');
        if(!($('#divInpPass > .invalid-feedback').length)) $('#divInpPass').append('<div class="invalid-feedback">Porfavor introduzca su contraseña</div>');
      }
      else if(username == "" || username == " "){
        $('#InputPass').removeClass('is-invalid');
        $('#InputPass').addClass('is-valid');

        $('#InputEmail').removeClass('is-valid');
        $('#InputEmail').addClass('is-invalid');
        if(!($('#divInpEmail > .invalid-feedback').length)) $('#divInpEmail').append('<div class="invalid-feedback">Porfavor introduzca su correo</div>');
      }
      else if(pass == "" || pass == " "){
        $('#InputEmail').removeClass('is-invalid');
        $('#InputEmail').addClass('is-valid');
        $('#InputPass').removeClass('is-valid');
        $('#InputPass').addClass('is-invalid');
        if(!($('#divInpPass > .invalid-feedback').length)) $('#divInpPass').append('<div class="invalid-feedback">Porfavor introduzca su contraseña</div>');
      }
      else{
          $('#InputEmail').removeClass('is-invalid');
          $('#InputPass').removeClass('is-invalid');
          $('.invalid-feedback').html('<div class="invalid-feedback"></div>');
          
          var dataString = 'username='+username+'&pass='+pass;
          $.ajax({
              type: "POST",
              url: "Models/Ajax/LogInModel.php",
              dataType: "json",
              data: dataString,
              success: function(data){
                $('#loader').fadeIn(1000).html(data);
                if(data.status == "error"){
                  $('#InputEmail').removeClass('is-valid');
                  $('#InputPass').removeClass('is-valid');
                  $('#InputEmail').addClass('is-invalid');
                  $('#InputPass').addClass('is-invalid');
                  Command: toastr["error"]('Correo o contraseña inválida.', "ERROR")
                }
                else if(data.status == "success"){
                  $('#InputEmail').removeClass('is-invalid');
                  $('#InputPass').removeClass('is-invalid');
                  $('#InputEmail').addClass('is-valid');
                  $('#InputPass').addClass('is-valid');
                  location.href='?controller=Home&action=index';
                  sleep(2000);
                }
                else if(data.status == "noactive"){
                  $('#InputEmail').removeClass('is-valid');
                  $('#InputPass').removeClass('is-valid');
                  $('#InputEmail').addClass('is-invalid');
                  $('#InputPass').addClass('is-invalid');
                  Command: toastr["error"]('Su correo no ha sido activado aún', "ERROR")
                }
              }
          });
      }		
    });
  $('#formEditar').validate({
    validClass: "is-valid",
    errorClass: "is-invalid",
    onkeyup: false,
    onclick: false,
    onfocusout: false,
    errorPlacement: function(label){
      return true;
    },
    highlight: function(label){
      $(label).removeClass("is-valid").addClass("is-invalid");
      $(label).parent().find('div.is-invalid').removeClass("valid-feedback").addClass("invalid-feedback");
    },
    unhighlight: function(label){
      $(label).removeClass("is-invalid").addClass("is-valid");
      $(label).parent().find('div.is-invalid').removeClass("invalid-feedback").addClass("valid-feedback");
    },
    submitHandler: function(form){
      $(form).submit();
    },
    rules: {
      nombreEdit: {
        required: true,
        minlength: 3,
        namelatam: true
      },

      apellidoEdit: {
        required: true,
        minlength: 3,
        namelatam: true
      }
    },
    messages: {
      nombreEdit: {
        required: function(){
          Command: toastr["error"]("Por favor introduzca un Nombre", "ERROR")
        },
        minlength: function(){
          Command: toastr["error"]("No se permiten nombres con menos de 3 caracteres", "ERROR")
        },
        namelatam: function(){
          Command: toastr["error"]("No se permiten números, ni caracteres especiales en este campo", "ERROR")
        }
      },
      apellidoEdit: {
        required: function(){
          Command: toastr["error"]("Por favor introduzca un Apellido", "ERROR")
        },
        minlength: function(){
          Command: toastr["error"]("No se permiten apellidos con menos de 3 caracteres", "ERROR")
        },
        namelatam: function(){
          Command: toastr["error"]("No se permiten números, ni caracteres especiales en este campo", "ERROR")
        }
      }
    }
  });

  $('#idEditar').on('click',function(){
    $('idEditPerf').insertBefore('<div style="width: 100%; height: 100%; position: absolute; top: 0; left: 0"><img class="img-logo" src="Views/Layouts/imgs/loader2.gif"></div>')
  })

  $('#oldPass').on('keypress', function() {
    $('#oldPass').removeClass('is-invalid');
    $('#newPass').removeClass('is-invalid');
  })

  $('#newPass').on('keypress', function() {
    $('#oldPass').removeClass('is-invalid');
    $('#newPass').removeClass('is-invalid');
  })

  $('#changePass').on('click', function(event) {
    event.preventDefault();
    event.stopPropagation();
    var oldPass = $('#oldPass').val();
    var newPass = $('#newPass').val();
    if((oldPass == "" || oldPass == " ") && (newPass == "" || newPass == " ")){
      $('#oldPass').removeClass('is-valid');
      $('#oldPass').addClass('is-invalid');

      $('#newPass').removeClass('is-valid');
      $('#newPass').addClass('is-invalid');

      Command: toastr["error"]('Si desea cambiar la contraseña debe llenar los dos campos', "ERROR")     
    }
    else if(oldPass == "" || oldPass == " "){
      $('#newPass').removeClass('is-invalid');
      $('#newPass').addClass('is-valid');

      $('#oldPass').removeClass('is-valid');
      $('#oldPass').addClass('is-invalid');
      Command: toastr["error"]('Si desea cambiar la contraseña debe indicar la antigua contraseña', "ERROR")
    }
    else if($('#newPass').is(":invalid")){
      $('#oldPass').removeClass('is-valid');
      $('#oldPass').removeClass('is-invalid');

      $('#newPass').removeClass('is-valid');
      $('#newPass').addClass('is-invalid');
      Command: toastr["error"]("La nueva contraseña debe tener por lo menos: 1 minúscula, 1 número y 1 mayúscula, se permiten caracteres especiales, pero no espacios", "ERROR")
    }
    else if(newPass == "" || newPass == " "){
      $('#oldPass').removeClass('is-invalid');
      $('#newPass').removeClass('is-valid');
      $('#newPass').addClass('is-invalid');
      Command: toastr["error"]('Si desea cambiar la contraseña debe indicar la nueva contraseña', "ERROR")
    }
    else{
        $('#loader').html('<img class="img-logo" src="Views/Layouts/imgs/loader2.gif" alt="">').fadeIn();
        $('#oldPass').removeClass('is-invalid');
        $('#newPass').removeClass('is-invalid');
        
        var dataString = 'oldPwd='+oldPass+'&newPwd='+newPass;
        $.ajax({
            type: "POST",
            url: "Models/Ajax/CambiarContra.php",
            dataType: "json",
            data: dataString,
            success: function(data){
              $('#loader').fadeIn(1000).html(data);
              if(data.status == "error"){
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-bottom-right",
                  "preventDuplicates": true,
                  "onclick": null,
                  "showDuration": "300",
                  "hideDuration": "1000",
                  "timeOut": 7000,
                  "extendedTimeOut": 2000,
                  "showEasing": "swing",
                  "showMethod": "fadeIn",
                  "tapToDismiss": true
                }
              Command: toastr["error"]('Contraseña actual incorrecta.', "ERROR")
              }
              else if(data.status == "success"){

                Command: toastr["success"]('Su contraseña ha sido cambiada correctamente.', "Cambio Realizado")
              }
            }
        });
    }		
  });

  $('#formNSolicitud').validate({
    validClass: "is-valid",
    errorElement: "div",
    errorClass: "is-invalid",
    onkeyup: function(element){$(element).valid()},
    success: function(label){
      label.html("Válido");
    },
    errorPlacement: function(label, element){
      label.insertAfter(element);
      label.addClass('invalid-feedback');
    },
    highlight: function(label){
      $(label).removeClass("is-valid").addClass("is-invalid");
      $(label).parent().find('div.is-invalid').removeClass("valid-feedback").addClass("invalid-feedback");
    },
    unhighlight: function(label){
      $(label).removeClass("is-invalid").addClass("is-valid");
      $(label).parent().find('div.is-invalid').removeClass("invalid-feedback").addClass("valid-feedback");
    },
    rules: {
      nombreSolicitante: {
        required: true,
        minlength: 3,
		    namelatam: true,
		    pattern: false
      },

      apellidoSolicitante: {
        required: true,
        minlength: 3,
		    namelatam: true,
		    pattern: false
      },
      nombreSolicitud: {
        required: true,
        minlength: 5,
		    pattern: false
      },

      lugarSolicitud: {
        required: true,
        minlength: 5,
		    pattern: false
      },

      tipoDaño: {
        required: true,
      }
    },
    messages: {
      nombreSolicitante: {
        required: "Por favor introduzca un Nombre",
        minlength: jQuery.validator.format("No se permiten nombres con menos de {0} caracteres"),
		    namelatam: "El campo nombre no debe llevar números ni caracteres especiales"
      },
      apellidoSolicitante: {
        required: "Por favor introduzca un Apellido",
        minlength: jQuery.validator.format("No se permiten apellidos con menos de {0} caracteres"),
		    namelatam: "El campo apellido no debe llevar números ni caracteres especiales"
      },
      nombreSolicitud: {
        required: "Por favor introduzca un Nombre para la solicitud",
        minlength: jQuery.validator.format("El nombre debe ser breve, pero no menos de {0} caracteres")
      },
      lugarSolicitud: {
        required: "Por favor especifique el lugar del daño",
        minlength: jQuery.validator.format("El lugar debe ser breve, pero no menos de {0} caracteres")
      },
      tipoDaño: {
        required: "Debe elegir el tipo de daño para un mejor seguimiento"
      }
    }
  })

  $('#codReenvio').on('click', function(){
    var cod = $('#codTemp').val();
    var dataString = 'cod='+cod;
    $.ajax({
      type: "POST",
      url: "Models/Ajax/ReenviarVerificacion.php",
      dataType: "json",
      data: dataString,
      success: function(data){
        $('#loader').fadeIn(1000).html(data);
        if(data.status == "success"){
          Command: toastr["success"]("Se logró enviar el correo de verificación.", "Realizado") 
        }
        else if(data.status == "error"){
          Command: toastr["error"]("Hubo un error al enviar el código al correo.", "ERROR")
        }
      }
    })
  })
  $(document).on('click','.borrar', function(event){
    event.preventDefault();
    var id = $(this).attr('id');
    var dataString = 'idSolMant='+id;
    $(this).closest('tr').remove();
    $.ajax({
      type: "POST",
      url: "Models/Ajax/ActualizarEstadoSolicitud.php",
      dataType: "json",
      data: dataString,
      success: function(data){
        $('#loader').fadeIn(1000).html(data);
        if(data.status == "error"){
          Command: toastr["error"]("Error al eliminar solicitud en la base de datos, recargue la página y si el problema persiste comuníquese con el administrador del sistema.", "ERROR")
        }
      }
    })
    if($('#tableContainer').find('tr').length == 0){
      $('#content').html('<div class="w-50 mt-5 bg-dark text-white d-flex justify-content-center align-items-center align-self-center mx-auto rounded-lg" style="height: 150px"><p>Sin Solicitudes</p></div>');
    }
  })

  $('#tableMant').on('change',function(){
    
    $('#tableMant').remove();
  })
  
});