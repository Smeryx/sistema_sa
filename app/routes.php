<?php


$controllers = array(
    'Home' => ['index'],
    'Usuarios' => ['login', 'perfil', 'edit', 'update', 'logout', 'signin', 'save', 'statusUp'],
    'Reparacion' => ['menu','formulario_de_actualizacion','materiales','reporte','nuevaSolicitud','enviarSolicitud','enviarSolicitudNoLog','mySolicitudes','mostrarSolicitudes','guardarSolicitudes','mantSolicitudes'],
    'Contactenos' => ['index']
);

if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
        call($controller, $action);
    } else {
        call('Home', 'index');
    }
} else {
    call('Home', 'index');
}

function call($controller, $action)
{


    require_once('Controllers/' . $controller . 'Controller.php');

    switch ($controller) {
        case 'Home':
            $controller = new HomeController();
            break;
        case 'Reparacion':
            $controller = new ReparacionController();
            break;
        case 'Usuarios':
            $controller = new UsuariosController();
            break;
        case 'Contactenos':
            $controller = new ContactenosController();
        default:
            # código...
            break;
    }

    $controller->{$action}();
}

?>