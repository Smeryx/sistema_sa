  <nav class="navbar navbar-expand-lg navbar-dark bg-dark mt-5 ml-5 mr-5">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">S.S.A</a>
  
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item <?php if($controller=='Home') echo 'active' ?>">
          <a class="nav-link" href="?controller=Home&action=index">Inicio</a>
        </li>
        <li class="nav-item <?php if($controller=='Reparacion') echo 'active' ?>">
          <a class="nav-link" href="?controller=Reparacion&action=menu">Reparación y Mantenimiento</a>
        </li>
        <li class="nav-item <?php if($controller=='Contactenos') echo 'active' ?>">
          <a class="nav-link" href="?controller=Contactenos&action=index">Contáctenos</a>
        </li>
      </ul>
    </div>
  </nav>