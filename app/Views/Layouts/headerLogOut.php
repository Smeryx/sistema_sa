<header class="navbar navbar-expand d-flex justify-content-between bg-green p-2">
    <div class="col-auto">
        <a href="?controller=Home&action=index">
            <div class="row">
                <div class="col-auto ml-1">
                    <img class="img-logo" src="Views/Layouts/imgs/FISC.png" alt="">
                </div>
                <div>
                    <span class="txt-white-sh">Sistema<br>Secretaría Administrativa</span>
                </div>
            </div>
        </a>
    </div>
    <div class="col-auto">
        <div class="flex-md-row">
            <a id="idLogin" href="?controller=Login&action=index"><div class="btn btn-outline-light mx-1">Iniciar Sesión</div></a>
            <a id="idSignIn" href="?controller=SignIn&action=index"><div class="btn btn-outline-light mx-1">Registrarse</div></a>
        </div>
    </div>
</header>