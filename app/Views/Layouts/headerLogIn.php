<header class="navbar navbar-expand d-flex justify-content-between bg-green p-2">
    <div class="col-auto">
        <a href="?controller=Home&action=index">
            <div class="row">
                <div class="col-auto ml-1">
                    <img class="img-logo" src="Views/Layouts/imgs/FISC.png" alt="">
                </div>
                <div>
                    <span class="txt-white-sh">Sistema<br>Secretaría Administrativa</span>
                </div>
            </div>
        </a>
    </div>
	<div class="col-auto">
        <div class="d-flex align-items-center">
            <a id="idLogin" href="?controller=Usuarios&action=perfil"><?php if($_GET['controller'] != "Usuarios") { ?><div class="text-white m-1 d-flex align-items-center" data-toggle="tooltip" data-placement="left" title="Ver Perfil"><img src="Views/Layouts/imgs/user.png" class="mx-2" style="width:30px" alt=""><h4 class="txt-shw"><?php echo $_SESSION['nombre']; ?></h4></div></a><?php } ?>
            <a id="idLogOut" href="?controller=Usuarios&action=logout"><div class="btn btn-outline-light ml-3 mr-1">Cerrar Sesión</div></a>
        </div>
    </div>
</header>