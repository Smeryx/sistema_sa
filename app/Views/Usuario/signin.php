<div class="container">
    <div class="row mb-5">
        <div class="col-md-2">
            <div class="position-fixed p-3 bg-light rounded-lg mt-5">
                Ya tengo cuenta <br> <a href="?controller=Home&action=index">Regresar</a>
            </div>
        </div>
        <div class="col-md-8">
        <form id ="formRegistro" class="needs-validation p-5 bg-light rounded-lg mt-5" action="?controller=Usuarios&action=save" method="POST" novalidate>
            <div class="d-flex justify-content-between mb-5">
                <h2>Registro de Usuario</h2>
                <div id="loader">

                </div>
            </div>
            
            
            <div class="form-row d-flex justify-content-center m-4">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="validationServerNombre">Nombre</label>
                        </div>
                        <div class="col-md">
                            <input type="text" class="form-control" id="validationServerNombre" name="nombre" placeholder="Nombre" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" required>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row d-flex justify-content-center m-4">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="validationServerApellido">Apellido</label>
                        </div>
                        <div class="col-md">
                            <input type="text" class="form-control" id="validationServerApellido" name="apellido" placeholder="Apellido" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" required>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group d-flex justify-content-center m-3">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="validationServerEmail">E-mail</label>
                            </div>
                            <div class="col-md">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend3">@</span>
                                    </div>
                                    <input type="email" class="form-control" name="mail" id="validationServerEmail" placeholder="E-mail" aria-describedby="inputGroupPrepend3" required>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row d-flex justify-content-center m-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="validationServerContraseña">Contraseña</label>
                            </div>
                            <div class="col-md">
                                <input type="password" value="<?php if(isset($_GET['error'])) echo $_SESSION['contraseña']; ?>" class="form-control" id="validationServerContraseña" name="contraseña" placeholder="Contraseña" required>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row d-flex justify-content-center m-4">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-2.5">
                                <label for="validationServerValContraseña">Confirmar Contraseña</label>
                            </div>
                            <div class="col-md">
                                <input type="password" class="form-control" id="validationServerValContraseña" name="valcontraseña" placeholder="Confirmar Contraseña" required>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center mr-3 mb-3">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-2.5">
                                <label for="validationServerNacimiento">Fecha de Nacimiento</label>
                            </div>
                            <div class="col-md">
                                <select class="form-control" id="validationServerNacimientoDia" name="dianac" required>
                                    <option value="">Dia</option>
                                    <?php for($dia = 1; $dia < 32; $dia++){
                                        echo "<option value='$dia'";
                                        echo ">$dia</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control" id="validationServerNacimientoMes" name="mesnac" required>
                                <option value="">Mes</option>
                                <?php
                                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                $cantidad = count($meses);
                                for($mes = 0; $mes<$cantidad; $mes++){
                                    $rmes = $mes + 1;
                                    echo "<option value='$rmes'"." ".$meses[$mes];
                                    if(isset($_GET['error'])){if($_SESSION['mesnac']==$rmes) echo " selected";}
                                    echo ">$meses[$mes]</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control" id="validationServerNacimientoAño" name="annac" required>
                                <option value="">Año</option>
                                <?php for($anio = ((int)date("Y")-99); $anio < (int)date("Y")-10; $anio++){
                                        echo "<option value='$anio'";
                                        if(isset($_GET['error'])){if($_SESSION['annac']==$anio) echo " selected";}
                                        echo ">$anio</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center mr-3 mb-3">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-2.5">
                                <label for="validationServerFacultad">Facultad</label>
                            </div>
                            <div class="col">
                                <select class="form-control" id="validationServerFacultad" name="facultad" required>
                                    <option value="">Escoja una Facultad</option>
                                    <?php
                                        $facultad = array("Ingeniería Civil","Ingeniería Mecánica","Ingeniería Eléctrica","Ingeniería Industria","Sistemas Computacionales","Ciencias y Tecnología");
                                        //$count = 0;
                                        $cantidad = count($facultad);
                                        for($count=0; $count<$cantidad; $count++){
                                            echo "<option value='$facultad[$count]'";
                                            echo ">$facultad[$count]</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group d-flex justify-content-center mr-3 mb-5">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2.5">
                                <label for="validationServerRol">Rol</label>
                            </div>
                            <div class="col">
                                <select class="form-control" id="validationServerRol" name="rol" required>
                                    <option value="">Escoja su rol en la institución</option>
                                    <?php
                                        $rol = array("Estudiante","Docente","Administrativo");
                                        $count = 0;
                                        $cantidad = count($rol);
                                        for($count=0; $count<$cantidad; $count++){
                                            echo "<option value='$rol[$count]'";
                                            if(isset($_GET['error'])){if($_SESSION['rol']==$rol[$count]) echo " selected";}
                                            echo ">$rol[$count]</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row d-flex justify-content-center mt-4 mx-2">
                    <button id="idRegistro" class="btn btn-dark" name='registro' type="submit">Registrarse</button>
                </div>
            </form>
            
        </div>
    </div>
    <div class="col-md-2"></div>
</div>