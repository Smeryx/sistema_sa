<?php if(isset($_SESSION['nombre'])){ ?>
<div id="idEditPerf" class="container bg-light rounded-lg my-5 p-1 pt-3">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex justify-content-center mb-5">
                <h2>Editar perfil</h2>
            </div>
            <div class="container">
                <form id="formEditar" class="d-flex justify-content-center mx-auto" action="?controller=Usuarios&action=update" method="POST">
                    <div class="d-flex align-items-center align-self-center flex-column mx-auto">
                        <div class="justify-content-center m-1">
                            <img src="Views/Layouts/imgs/user.png" class="img-logo-bienvenido" alt="">  
                        </div>
                        <div class="justify-content-center m-1 my-5">
                            <button id="idEditar" class="btn btn-primary m-2" name='editar' type="submit" value="changeInfo">Guardar Cambios</button>
                            <a href="?controller=Usuarios&action=perfil"><div class="btn btn-danger m-2">Cancelar y regresar</div></a>
                        </div>
                    </div>
                    <div class="d-flex mx-auto">
                        <table class="table table-dark p-2 rounded-lg">
                            <tbody>
                                <tr>
                                    <th scope="row">Nombre:</th>
                                    <td><input type="text" id="userNombre" name="nombreEdit" value="<?php echo $_SESSION['nombre']; ?>" class="form-control" required></td>
                                </tr>
                                <tr>
                                    <th scope="row">Apellido:</th>
                                    <td><input type="text" id="userApellido" name="apellidoEdit" value="<?php echo $_SESSION['apellido']; ?>" class="form-control" required></td>
                                </tr>
                                <tr>
                                    <th scope="row">Correo:</th>
                                    <td><?php echo $_SESSION['email']; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Fecha de Nacimiento:</th>
                                    <td>
                                        <div class="d-flex justify-items-center">
                                        <select class="form-control mx-1" id="NacimientoDia" name="dianacEdit" required>
                                            <?php for($dia = 1; $dia < 32; $dia++){
                                                echo "<option value='$dia'";
                                                if($_SESSION['dia_nac']==$dia) echo " selected";
                                                echo ">$dia</option>";
                                                }
                                            ?>
                                        </select>
                                    <select class="form-control" id="NacimientoMes" name="mesnacEdit" required>
                                    <?php
                                    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                    $cantidad = count($meses);
                                    for($mes = 0; $mes<$cantidad; $mes++){
                                        $rmes = $mes + 1;
                                        echo "<option value='$rmes'"." ".$meses[$mes];
                                        if($_SESSION['mes_nac']==$rmes) echo " selected";
                                        echo ">$meses[$mes]</option>";
                                    }
                                    ?>
                                    </select>
                                    <select class="form-control mx-1" id="NacimientoAño" name="annacEdit" required>
                                    <?php for($anio = ((int)date("Y")-99); $anio < (int)date("Y")-10; $anio++){
                                            echo "<option value='$anio'";
                                            if($_SESSION['an_nac']==$anio) echo " selected";
                                            echo ">$anio</option>";
                                            }
                                        ?>
                                    </select>
                                    </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Facultad:</th>
                                    <td><select class="form-control mx-1" id="Facultad" name="facultadEdit" required>
                                        <?php
                                            $facultad = array("Ingeniería Civil","Ingeniería Mecánica","Ingeniería Eléctrica","Ingeniería Industria","Sistemas Computacionales","Ciencias y Tecnología");
                                            $cantidad = count($facultad);
                                            for($count=0; $count<$cantidad; $count++){
                                                echo "<option value='$facultad[$count]'";
                                                if($_SESSION['facultad']==$facultad[$count]) echo " selected";
                                                echo ">$facultad[$count]</option>";
                                            }
                                        ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Rol:</th>
                                    <td><?php if(!($_SESSION['rol'] == "Secretaria" || $_SESSION['rol'] == "Personal Mantenimiento")) { ?>
                                        <select class="form-control" id="Rol" name="rolEdit" required>
                                            <?php
                                                $rol = array("Estudiante","Docente","Administrativo");
                                                $cantidad = count($rol);
                                                for($count=0; $count<$cantidad; $count++){
                                                    echo "<option value='$rol[$count]'";
                                                    if($_SESSION['rol']==$rol[$count]) echo " selected";
                                                    echo ">$rol[$count]</option>";
                                                }
                                            ?>
                                        </select>
                                            <?php }
                                            else{
                                                echo $_SESSION['rol'];
                                            }?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
                <hr>
                <div class="row">
                    <div class="col-md"></div>
                    <form id="formChangePass" class="col-md-6 my-3">
                        <table class="table table-dark p-2 rounded-lg">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Cambio de Contraseña</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th scope="row">Contraseña actual</th>
                                <td><input id="oldPass" name="oldPwd" type="password" value="" class="form-control"></td>
                                </tr>
                                <tr>
                                <th scope="row">Contraseña nueva</th>
                                <td><input id="newPass" name="newPwd" type="password" pattern="^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040]*)(?=.*[A-Z])(?=.*[a-z])\S{8,16}$" class="form-control"></td>
                                </tr>
                                <tr>
                                <td><button id="changePass" class="btn btn-primary" type="submit" value="changePass">Cambiar contraseña</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    <div id="loader" class="col-md"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
else{
    echo "<script> location.href='?controller=Home&action=index' </script>";
}