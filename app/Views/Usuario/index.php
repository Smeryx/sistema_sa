<!DOCTYPE html>
<html lang="es">
<head>
  <title>Sistema Secretaría Administrativa</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../../node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../node_modules/myStyle/mycss/index-style.css">
  <link rel="shortcut icon" href="../imgs/FISC.png" type="image/x-icon">
</head>
<body>
    <?php require_once('Views/Layouts/headerNoLog.php');?>
    <div class="container">
        <form class="p-5 bg-light rounded-lg mt-5" action="?controller='Login'&action='login'" method="POST">
            <div class="mb-5">
                <h2>Iniciar Sesión</h2>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Correo</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Introduzca el correo">
                <small id="emailHelp" class="form-text text-muted">Ejemplo: nombre.apellido@utp.ac.pa</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Contraseña</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Introduzca la contraseña">
            </div>
            <button type="submit" class="btn btn-dark">Entrar</button> 
        </form>
    </div>
</body>
</html>
