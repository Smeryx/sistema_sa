<div class="container">
    <div class="row mb-5">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        <form class="pt-5 px-5 pb-2 bg-light rounded-lg mt-5" action="" method="POST">
            <div class="d-flex justify-content-between mb-5">
                <div>
                    <h2>Iniciar Sesión</h2>
                </div>
                <div id="loader">
                    
                </div>
            </div>
            <div class="form-row d-flex justify-content-center m-4">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-auto">
                            <label for="InputEmail">Correo</label>
                        </div>
                        <div id="divInpEmail" class="col-md">
                            <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Introduzca el correo" required>
                            <small id="emailHelp" class="form-text text-muted">Ejemplo: nombre.apellido@utp.ac.pa</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row d-flex justify-content-center m-4">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-auto">
                            <label for="InputPass">Contraseña</label>
                        </div>
                        <div id="divInpPass" class="col-md">
                            <input type="password" class="form-control" id="InputPass" placeholder="Introduzca su contraseña" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row d-flex justify-content-center m-5">
                <button id="btnLogin" type="submit" class="btn btn-dark">Iniciar Sesión</button> 
            </div>
            <div class="form-group">
                ¿No tienes cuenta? <a href="?controller=Usuarios&action=signin">Registrate aquí</a>
            </div>
        </form>
        </div>
        <div class="col-md-2"></div>
    </div>
</div> 
