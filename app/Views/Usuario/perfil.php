<?php if(isset($_SESSION['nombre'])){ ?>
<div class="container bg-light rounded-lg mt-5 p-3 t-">
    <div class="row">
        <div class="col-md-2">
            <a href="?controller=Home&action=index"><div class="btn btn-dark ml-2">Regresar</div></a>
        </div>
        <div class="col-md-8">
            <div>
                <div class="d-flex justify-content-center mb-5">
                    <h2>Perfil de Usuario</h2>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="d-flex align-items-center align-self-center flex-column mx-auto">
                            <div class="justify-content-center m-1">
                                <img src="Views/Layouts/imgs/user.png" class="img-logo-bienvenido" alt="">  
                            </div>
                            <div class="ustify-content-center m-1 my-5">
                                <a href="?controller=Usuarios&action=edit"><div class="btn btn-primary">Editar Perfiil</div></a>
                            </div>
                        </div>
                        <div class="mx-auto">
                        <table class="table table-dark rounded-lg">
                            <tbody>
                                <tr>
                                    <th scope="row">Nombre:</th>
                                    <td><?php echo $_SESSION['nombre']; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Apellido:</th>
                                    <td><?php echo $_SESSION['apellido']; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Correo:</th>
                                    <td><?php echo $_SESSION['email']; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Fecha de Nacimiento:</th>
                                    <?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); ?>
                                    <td><?php echo $_SESSION['dia_nac']." de ".$meses[$_SESSION['mes_nac']-1]." de ".$_SESSION['an_nac']; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Facultad:</th>
                                    <td><?php echo $_SESSION['facultad']; ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Rol:</th>
                                    <td><?php echo $_SESSION['rol']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<?php }
else{
    echo "<script> location.href='?controller=Home&action=index' </script>";
}
