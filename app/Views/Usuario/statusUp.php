<?php
    if($status == 1){
?>
    <div class="container ">
        <div class="row mb-5">
            <div class="col-md-2"></div>
            <div class="col-md-8 bg-light rounded-lg mt-5 p-4" align="center">
                <h1>Cuenta activada exitosamente</h1>
                <img class="my-3" src="Views/Layouts/imgs/check.png" style="width: 200px; height: 200px" alt="">
                <div class="col mt-3">
                    <a href="?controller=Home&action=index"><div class="btn btn-dark m-2">Regresar a Inicio</div></a>
                </div>
                
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
<?php    
    }
    else{
?>
    <div class="container ">
        <div class="row mb-5">
            <div class="col-md-2"></div>
            <div class="col-md-8 bg-light rounded-lg mt-5 p-4" align="center">
                <h1 class="text-danger">Error de código</h1>
                <img class="my-3" src="Views/Layouts/imgs/error.png" style="width: 200px; height: 200px" alt="">
                <p>El link para verificar un correo no posee un código de activación correcto</p>
                <div class="col mt-3">
                    <a href="?controller=Home&action=index"><div class="btn btn-dark m-2">Regresar a Inicio</div></a>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
<?php   
    }