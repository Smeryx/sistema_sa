<div class="container-fluid">
   <div class="row m-2">
      <div class="col-md-2"></div> 
      <div class="col-md-8 container-fluid bg-light mx-0 mt-5">
         <div class="col-md mx-auto">
            <h2 class="mt-5 ml-3"> Formulario De Solicitud</h2>
            <div class="container mb-5 p-4">
               <?php if(!(isset($_SESSION['nombre']))){
                  ?><form id="formNSolicitud" class="form-horizontal" action="?controller=Reparacion&action=enviarSolicitudNoLog" method="POST" novalidate>
                    <div class="form-group">
                     <label class="col-md-4 control-label">Nombre del Solicitante</label>
                     <div class="col-md-8 inputGroupContainer">
                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input name="nombreSolicitante" placeholder="Nombre" class="form-control" type="text" required></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="col-md-4 control-label">Apellido del Solicitante</label>
                     <div class="col-md-8 inputGroupContainer">
                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input name="apellidoSolicitante" placeholder="Nombre" class="form-control" type="text" required></div>
                     </div>
                  </div>
              <?php }
               else{ ?>
                  <form id="formNSolicitud" class="form-horizontal" action="?controller=Reparacion&action=enviarSolicitud" method="POST" novalidate><?php
               }?>   
                  <div class="form-group">
                     <label class="col-md-4 control-label">Nombre de la solicitud:</label>
                     <div class="col-md-8 inputGroupContainer">
                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span><input id="Nombre" name="nombreSolicitud" placeholder="Nombre Solicitud" class="form-control" type="text" required></div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-md-4 control-label">Lugar de daño:</label>
                     <div class="col-md-8 inputGroupContainer">
                        <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span><input id="lugar" name="lugarSolicitud" placeholder="Escriba el lugar del daño" class="form-control" type="text" required></div>
                     </div>
                  </div>

               
                  <div class="form-group">
                     <label class="col-md-4 control-label">Tipo de daño:</label>
                     <div class="col-md-8 inputGroupContainer">
                        <div class="input-group">
                           <span class="input-group-addon" style="max-width: 100%;"><i class="glyphicon glyphicon-list"></i></span>
                           <select class="selectpicker form-control" name="tipoDaño">
                              <option value="">Seleccione el tipo de daño</option>
                              <option value="Eléctrico">Eléctrico</option>
                              <option value="Aires acondicionado">Aires acondicionado</option>
                              <option value="Mobiliario">Mobiliario</option>
                              <option value="Equipo">Equipo</option>
                              <option value="Infraestructura">Infraestructura</option>
                              <option value="Elevadores">Elevadores</option>
                              <option value="Otros">Otros</option>
                           </select>
                        </div>
                     </div>
                  </div>

                  <div class="form-group">
                     <label class="col-md-4 control-label">Descripción:</label>
                     <div class="col-md-8 inputGroupContainer">
                        <div  class="form-group">
                           <label for="exampleFormControlTextarea1"></label>
                           <textarea class="form-control" id="exampleFormControlTextarea1" name="descr" rows="5" placeholder="Escriba una breve descripcion del daño"></textarea>
                        </div>   
                     </div>
                  </div>

                  <div class="form-group col-md-4">
                     <button id="btnNuevaSolicitud" class="btn btn-success" type="Submit">Enviar</button>
                  </div>
               </form>
            
            </div>
         </div>
      </div>
      <div class="col-md-2"></div>
   </div>
 </div>


