<div class="container ">
    <div class="row mb-5">
        <div class="col-md-2"></div>
        <div class="col-md-8 bg-light rounded-lg mt-5 p-4" align="center">
            <h1>Solicitud Enviada</h1>
            <img class="my-3" src="Views/Layouts/imgs/check.png" style="width: 200px; height: 200px" alt="">
            <div>
                <a href="?controller=Home&action=index"><div class="btn btn-dark m-2">Regresar a Inicio</div></a>
                <a href="?controller=Reparacion&action=menu"><div class="btn btn-dark m-2">Regresar a Menú Reparación</div></a>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>