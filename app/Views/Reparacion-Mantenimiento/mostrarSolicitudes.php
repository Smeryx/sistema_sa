<div class="container-fluid mt-5">
  <div class="p-3 mb-2 bg-light text-dark scrolling-wrapper" style="overflow-x: scroll">
    <div class="container my-4 mb-5">
      <h1>Solicitudes de Reparación</h1>
    </div>
    <form action="?controller=Reparacion&action=guardarSolicitudes" method="POST">
      <div>
        <?php if(empty($lista)){ ?>
          <div class="w-50 mt-5 bg-dark text-white d-flex justify-content-center align-items-center align-self-center mx-auto rounded-lg" style="height: 150px"><p>Sin Solicitudes</p></div>
        <?php }
        else{ ?>
        <table class="table table-bordered table-striped">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id Solicitud</th>
              <th scope="col">Nombre de la solicitud</th>
              <th scope="col">Lugar del daño</th>
              <th scope="col">Tipo de daño</th>
              <th scope="col">Descripción</th>
              <th scope="col">Fecha de la solicitud</th>
              <th scope="col">Solicitante</th>
              <th scope="col" width="160">Estado</th>
            </tr>
          </thead>
          <tbody>
            <?php $estados = array("Sin Aprobar", "Aprobar", "Rechazar");
            $cantidad = count($estados);
            foreach($lista as $clave){
              $fecha =  date("d/m/Y",strtotime($clave['fecha']));
              if(empty($clave['descr'])){
                $descr = "Sin descripción";
              }
              else{
                $descr = $clave['descr'];
              }
            echo "<tr>";
            echo  '<th scope="row"><input class="d-none" name="id[]" value="'.$clave['id'].'">'.$clave['id'].'</th>';
            echo  "<td>".$clave['nombre']."</td>";
            echo  "<td>".$clave['lugar']."</td>";
            echo  "<td>".$clave['tipo']."</td>";
            echo  "<td>".$descr."</td>";
            echo  "<td>".$fecha."</td>";
            echo  "<td>".$clave['solicitante']."</td>"; 
            if($clave['estado']!=3){ ?>
            <td><select class="form-control" name="estado[]"><?php
                for($count=0; $count<$cantidad; $count++){
                  echo "<option value='$count'";
                  if($clave['estado'] == $count) echo " selected";
                  echo ">$estados[$count]</option>";
              }
            ?></select></td><?php }
            else{
              echo '<td class="bg-success text-white d-flex justify-content-center align-items-stretch"><input class="d-none" name="estado[]" value="'.'3'.'"><p class="my-3">Finalizado</p></td>';
            }
            echo "</tr>";
            } ?>
          </tbody>
        </table>
        <?php } ?>
      </div>
      <div class="container mt-5">
      
        <a href="?controller=Reparacion&action=menu">
          <div class="btn btn-dark m-2" value="Revisar Solicitudes">Volver al Menú</div>
        </a>
        <button id="btnSoli" class="btn btn-primary m-2" name='saveSoli' type="submit">Guardar Cambios</button>
    
      </div>
    </form>
  </div>
</div>