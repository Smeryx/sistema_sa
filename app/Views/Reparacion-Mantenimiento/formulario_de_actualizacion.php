<div class="mx-auto" style="width: 1000px">
<div class="p-3 mb-2 mt-5 bg-light text-dark">
  
  <form class="container">
  <div >
        <h2>Solicitud: </h2> 
</div>
  <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Técnico #1</span>
  </div>
  <input type="text" aria-label="First name" class="form-control" placeholder="Nombre">
  <input type="text" aria-label="Last name" class="form-control" placeholder="Apellido">
</div>
  
  <div class="input-group mt-3">
  <div class="input-group-prepend">
    <span class="input-group-text">Técnico #2</span>
  </div>
  <input type="text" aria-label="First name" class="form-control" placeholder="Nombre">
  <input type="text" aria-label="Last name" class="form-control" placeholder="Apellido">
</div>
<div class="form-group d-flex justify-content-center mr-3 mb-3 mt-3">
                    <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-2.5">
                                <label for="validationServerNacimiento">Fecha de Nacimiento</label>
                            </div>
                            <div class="col-md">
                                <select class="form-control" id="validationServerNacimientoDia" name="dianac" required>
                                    <option value="">Dia</option>
                                    <?php for($dia = 1; $dia < 32; $dia++){
                                        echo "<option value='$dia'";
                                        echo ">$dia</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control" id="validationServerNacimientoMes" name="mesnac" required>
                                <option value="">Mes</option>
                                <?php
                                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                                $cantidad = count($meses);
                                for($mes = 0; $mes<$cantidad; $mes++){
                                    $rmes = $mes + 1;
                                    echo "<option value='$rmes'"." ".$meses[$mes];
                                    if(isset($_GET['error'])){if($_SESSION['mesnac']==$rmes) echo " selected";}
                                    echo ">$meses[$mes]</option>";
                                }
                                ?>
                                </select>
                            </div>
                            <div class="col">
                                <select class="form-control" id="validationServerNacimientoAño" name="annac" required>
                                <option value="">Año</option>
                                <?php for($anio = ((int)date("Y")-99); $anio < (int)date("Y")-10; $anio++){
                                        echo "<option value='$anio'";
                                        if(isset($_GET['error'])){if($_SESSION['annac']==$anio) echo " selected";}
                                        echo ">$anio</option>";
                                        }
                                    ?>
                                </select>
                            </div>


   <div class="container mt-3">
<div class="btn-group mr-2" role="group" aria-label="First group">
<button type="button" class="btn btn-dark"
    <?php if($controller=='Reparacion') echo 'active' ?>   
    value="Revisar Solicitudes"
    onclick="location.href='?controller=Reparacion&action=materiales'" >
    Siguiente
</div>
    
<div class="btn-group mr-2" role="group" aria-label="Second group">
    <button type="button" class="btn btn-dark"
    <?php if($controller=='Reparacion') echo 'active' ?>   
    value="Revisar Solicitudes"
    onclick="location.href='?controller=Reparacion&action=menu'" >
    Cancelar
</div>
</div>
</div>
    
</form>
  </div>
  </div>
    
    