<div class="container-fluid">
  <div class="container sticky-top">
  <?php require_once('Views/Layouts/navbar.php');?>
</div>
  <div class="row">
  <div class="col-md-1 mx-3"> </div> 
  <div class="col-md-5 container-fluid bg-white-opa-7 mx-0 mt-5">
    <h2 class="mt-5 ml-3"> Menú de solicitudes</h2>
    <div class="container mt-5 mb-5 pt-3">
      <?php if(isset($_SESSION['nombre'] ) && $_SESSION['rol'] == "Personal Mantenimiento"){ ?>

        <a href="?controller=Reparacion&action=mantSolicitudes">
          <div class="btn btn-success btn-lg btn-block my-1">Listar Solicitudes</div>
        </a>

      <?php } 
        else{ ?>
      
      <a href="?controller=Reparacion&action=nuevaSolicitud">
        <div class="btn btn-success btn-lg btn-block my-1">Generar Nueva Solicitud</div>
      </a>
      <?php if(isset($_SESSION['nombre'] ) && $_SESSION['rol'] == "Secretaria"){ ?>
        <a href="?controller=Reparacion&action=mostrarSolicitudes">
          <div class="btn btn-success btn-lg btn-block my-1">Listar Solicitudes</div>
        </a>
      <?php } ?>
      <?php if(isset($_SESSION['nombre'] )){ ?>
        <a href="?controller=Reparacion&action=mySolicitudes">
          <div class="btn btn-success btn-lg btn-block my-1">Mis Solicitudes</div>
        </a>
      <?php } 
        }
      ?>
    </div>
  </div>
  <div class="col-md-4 container-fluid mt-5">
    <img src="Views/Layouts/imgs/solicitud.png" class="img-fluid" alt="Responsive image">
  </div>
  <div class="col-md-1"></div>
</div>
 </div>
