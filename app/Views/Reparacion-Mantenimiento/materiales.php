<div class="mx-auto" style="width: 1000px">
<div class="p-3 mb-2 bg-light text-dark">
<div>
    <h2>Solicitud: </h2>
</div>
<div>
    <h3>Materiales y estado de la solicitud</h3>
</div>
<div>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Cantidad</th>
      <th scope="col">Descripción</th>
      <th scope="col">Requeridos por</th>
      <th scope="col">Fecha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
</div>
<div class="container mt-3">
<div class="btn-group">
  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Actualizar Estado
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="#">Revisado</a>
    <a class="dropdown-item" href="#">Pospuesto</a>
    <a class="dropdown-item" href="#">Arreglado</a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#">Separated link</a>
  </div>
</div>

</div>
<div class="container mt-3">
    <h3>Observaciones</h3>
</div>
<div  class="form-group">
<label for="exampleFormControlTextarea1"></label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div>
<div class="container mt-3">
<div class="btn-group mr-2" role="group" aria-label="First group">
<button type="button" class="btn btn-dark"
    <?php if($controller=='Reparacion') echo 'active' ?>   
    value="Revisar Solicitudes"
    onclick="location.href='?controller=Reparacion&action=reporte'" >
    Agregar materiales y ver reporte
</div>
    
<div class="btn-group mr-2" role="group" aria-label="Second group">
    <button type="button" class="btn btn-dark"
    <?php if($controller=='Reparacion') echo 'active' ?>   
    value="Revisar Solicitudes"
    onclick="location.href='?controller=Reparacion&action=menu'" >
    Cancelar
</div>
</div>
</div>

</div>
</div>