<div class="container mt-5">
<div class="p-3 mb-2 bg-light text-dark">
<div class="container mt-3">
    <h2>Solicitud: </h2>
</div>
<div class="container mt-3">
    <h5>Estato: </h5>
</div>
<div class= "container mt-3">
    <h5>Fecha:</h5>
</div>
<div class= "container mt-3">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Cantidad</th>
      <th scope="col">Descripción</th>
      <th scope="col">Requeridos por</th>
      <th scope="col">Fecha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
</div>
<div class= "container mt-3">
    <h5>Observaciones:</h5>
</div>
<div  class="form-group">
<label for="exampleFormControlTextarea1"></label>
    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div>
<div class= "container mt-3">
    <h5>Realizado por: </h5>
</div>
<div class="container mt-5">
<div class="btn-group mr-2" role="group" aria-label="First group">
<button type="button" class="btn btn-dark"
    <?php if($controller=='Reparacion') echo 'active' ?>   
    value="Revisar Solicitudes"
    onclick="location.href='?controller=Reparacion&action=solicitud'" >
    Ver otras solicitudes
</div>
    
<div class="btn-group mr-2" role="group" aria-label="Second group">
    <button type="button" class="btn btn-dark"
    <?php if($controller=='Reparacion') echo 'active' ?>   
    value="Revisar Solicitudes"
    onclick="location.href='?controller=Reparacion&action=materiales'" >
    Editar
</div>
</div>
</div>

</div>
</div>