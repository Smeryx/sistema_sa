<div class="container-fluid mt-5">
  <div class="p-3 mb-2 bg-light text-dark">
    <div class="container my-4 mb-5">
      <h1>Mis Solicitudes de Reparación</h1>
    </div>
    <div>
      <?php if(empty($milista)){ ?>
        <div class="w-50 mt-5 bg-dark text-white d-flex justify-content-center align-items-center align-self-center mx-auto rounded-lg" style="height: 150px"><p>Sin Solicitudes</p></div>
      <?php }
       else{ ?>
      <table class="table table-bordered table-striped">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Id Solicitud</th>
            <th scope="col">Nombre de la solicitud</th>
            <th scope="col">Lugar del daño</th>
            <th scope="col">Tipo de daño</th>
            <th scope="col">Descripción</th>
            <th scope="col">Fecha de la solicitud</th>
            <th scope="col">Estado</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($milista as $clave){
            $fecha =  date("d/m/Y",strtotime($clave['fecha']));
            if(empty($clave['descr'])){
              $descr = "Sin descripción";
            }
            else{
              $descr = $clave['descr'];
            }
          echo "<tr>";
          echo  '<th scope="row">'.$clave['id'].'</th>';
          echo  "<td>".$clave['nombre']."</td>";
          echo  "<td>".$clave['lugar']."</td>";
          echo  "<td>".$clave['tipo']."</td>";
          echo  "<td>".$descr."</td>";
          echo  "<td>".$fecha."</td>";
          if($clave['estado'] == 0) echo  "<td>Sin Aprobar</td>";
          else if($clave['estado'] == 1) echo  '<td class="d-flex justify-content-center align-items-stretch bg-warning" style="font-weight: bold"><p class="my-3">Aprobado: Seguimiento</p></td>';
          else if($clave['estado'] == 3) echo  '<td class="bg-success text-white d-flex justify-content-center align-items-stretch"><p class="my-3">Aprobado: Finalizado</p></td>';
          else if($clave['estado'] == 2) echo  '<td class="bg-danger text-white d-flex justify-content-center align-items-stretch"><p class="my-3">Rechazado</p></td>';
          echo "</tr>";
          } ?>
        </tbody>
      </table>
      <?php } ?>
    </div>
    <div class="container mt-5">
      <div class="btn-group mr-2" role="group" aria-label="Second group">
        <a href="?controller=Reparacion&action=menu">
          <div class="btn btn-dark" value="Revisar Solicitudes">Volver al Menú</div>
        </a>
      </div>
    </div>
  </div>
</div>