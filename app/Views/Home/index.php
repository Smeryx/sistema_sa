<?php 
    if(!(isset($_SESSION['nombre']))){
      require_once('Views/Usuario/login.php');
    }
    else{
?>
      <div class="container ">
      <div class="row mb-5">
        <div class="col-md-2"></div>
        <div class="col-md-8 bg-light rounded-lg mt-5 p-4" align="center">
            <h1>Bienvenido <?php echo $_SESSION['nombre']." ".$_SESSION['apellido']; ?></h1>
            <img class="img-logo-bienvenido mt-5 mb-3" src="Views/Layouts/imgs/FISC.png" alt="">
        </div>
        <div class="col-md-2"></div>
        </div>
      </div>
<?php
    }
?>
