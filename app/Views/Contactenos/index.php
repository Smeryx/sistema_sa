<div class="container bg-light rounded-lg my-5 p-4 pt-3">
    <div class="row">
        <div class="col-md-12">
            <div class="d-flex mb-5">
                <h2>Información de Contácto</h2>
            </div>
            <div class="bg-dark rounded-lg p-3 text-white">
                <h5 class="mb-3">Personal de la Secretaría Administrativa</h5>
                <div class="d-flex align-items-center flex-column mt-3">
                    <p class="m-0">Lic. Marisol Cedeño</p>
                    <p class="m-0">Secretaria Administrativa</p>
                    <p class="m-0 text-success">marisol.cedeno@utp.ac.pa</p>
                </div>
                <hr class="bg-light">
                <div class="d-flex align-items-center flex-column mt-3">
                    <p class="m-0">Claribel Pimentel</p>
                    <p class="m-0">Asistente de Secretaría</p>
                    <p class="m-0 text-success">claribel.pimentel@utp.ac.pa</p>
                </div>
            </div>
        </div>
    </div>
</div>