<!DOCTYPE html>
<html lang="es">
<head>
  <title>Sistema Secretaría Administrativa</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="mycss/index-style.css">
  <link rel="shortcut icon" href="website/imgs/FISC.png" type="image/x-icon">
</head>
<body>
    <?php 
        include 'website/pagesHeader/headerNoLog.html'
    ?>
    <div class="container">
        <form class="p-5 bg-light rounded-lg mt-5" action="" method="POST">
            <div class="mb-5">
                <h2>Registrarse</h2>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="validationServer01">Nombre</label>
                    <input type="text" class="form-control" id="validationServer01" placeholder="Nombre" value="" required>
                    <div class="valid-feedback">
                        Válido
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationServer02">Apellido</label>
                    <input type="text" class="form-control" id="validationServer02" placeholder="Apellido" value="" required>
                    <div class="valid-feedback">
                        Válido
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="validationServerUsername">Usuario</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend3">@</span>
                        </div>
                        <input type="text" class="form-control" id="validationServerUsername" placeholder="Usuario" aria-describedby="inputGroupPrepend3" required>
                        <div class="invalid-feedback">
                            Porfavor escoge un nombre de usuario
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                <label for="validationServer03">Facultad</label>
                <input type="text" class="form-control" id="validationServer03" placeholder="Facultad" required>
                <div class="invalid-feedback">
                    Porfavor escoge una facultad
                </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="invalidCheck3" required>
                <label class="form-check-label" for="invalidCheck3">
                    Acepto los términos y condiciones
                </label>
                <div class="invalid-feedback">
                    Debes aceptar los términos y condiciones antes de registrarte
                </div>
                </div>
            </div>
            <button class="btn btn-dark" type="submit">Registrarse</button>
        </form>
    </div>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/jquery/jquery.min.js"></script>
</body>
</html>