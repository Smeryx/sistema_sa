<?php session_start(); ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <title>Sistema Secretaría Administrativa</title>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="node_modules/toastr/build/toastr.min.css">
  <link rel="stylesheet" type="text/css" href="myStyle/mycss/index-style.css">
  <link rel="shortcut icon" href="Views/Layouts/imgs/FISC.png" type="image/x-icon">
</head>
<?php
/*Por medio de este archivo se accede a la aplicación*/
require_once("Db/db.php"); //Se incluye el archivo que contiene la conexión a la BD
if(isset($_SESSION['nombre'])){
    require_once('Views/Layouts/headerLogIn.php');
}
else{
    require_once('Views/Layouts/headerNoLog.php');
}
if (isset($_GET['controller']) && isset($_GET['action'])) { // Si hay un controlador y acción (método) definido...
    /*if($_GET['controller']=="Sign" && $_GET['action']=="save"){
        $controller = 'Home';
        $action = 'index';
    }*/
    $controller = $_GET['controller'];
    $action = $_GET['action'];
} else { //Si no se ha definido un controlador y acción (caso cuando se accede la primera vez en la sesión)
    $controller = 'Home';
    $action = 'index';
}
require_once("Views/Layouts/layout.php");
?>