<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';


class UsuariosModel{
    private $db;
    private $usuarios;
 
    public function __construct(){
        $this->db = Conexion::conectar();
        $this->usuarios = array();
    }
    public function listar(){
        $consulta = $this->db->query("select * from Usuarios;");
        while($filas = $consulta->fetch_assoc()){
            $this->usuarios[] = $filas;
        }
        return $this->usuarios;
    }

    public function update_info(){
        $id = (int) $_SESSION['id'];
        $nombre = trim($_POST['nombreEdit']);
        $apellido = trim($_POST['apellidoEdit']);
        $fecha = $_POST['annacEdit']."-".$_POST['mesnacEdit']."-".$_POST['dianacEdit'];
        $facultad = $_POST['facultadEdit'];
        $rol = $_POST['rolEdit'];

        $this->db->query("UPDATE Usuarios SET us_nombre = '$nombre', us_apellido = '$apellido', us_nacimiento = '$fecha', us_facultad = '$facultad', us_rol = '$rol' WHERE id_usuario = $id");

        $result = $this->db->query("SELECT us_nombre, us_apellido, us_email, us_nacimiento, us_facultad, us_rol FROM Usuarios WHERE id_usuario = $id");

        while($fila = $result->fetch_assoc()){
            $_SESSION['id'] = $id;
            $_SESSION['nombre'] = $fila["us_nombre"];
            $_SESSION['apellido'] = $fila["us_apellido"];
            $_SESSION['email'] = $fila["us_email"];
            $fecha = strtotime($fila["us_nacimiento"]);
            $_SESSION['dia_nac'] = date("d", $fecha);
            $_SESSION['mes_nac'] = date("m", $fecha);
            $_SESSION['an_nac'] = date("Y", $fecha);
            $_SESSION['facultad'] = $fila["us_facultad"];
            $_SESSION['rol'] = $fila["us_rol"];
        }
    
    }
    public function registrar(){
        $nombre = trim($_POST['nombre']);
        $apellido = trim($_POST['apellido']);
        $email = trim($_POST['mail']);
        $pass = md5(trim($_POST['contraseña']));
        $fecha = $_POST['annac']."-".$_POST['mesnac']."-".$_POST['dianac'];
        $facultad = $_POST['facultad'];
        $rol = $_POST['rol'];
        $querySelect = $this->db->query("Select * from Usuarios Where us_email = '$email' ");
        if($querySelect->fetch_assoc()>0){
            echo "ERROR AL GUARDAR REGISTRO, CONSULTAR CON EL ADMINISTRADOR";
        }
        else{
            $querySelect->free();
            $cod = md5(random_bytes(10));
            $this->db->query("insert into Usuarios(us_nombre, us_apellido, us_email, us_pass, us_nacimiento, us_facultad, us_rol, us_status, us_codpase) values ('$nombre','$apellido','$email','$pass','$fecha','$facultad','$rol',0,'$cod')");
            $reg = $this->db->query("Select id_usuario from Usuarios Where us_email = '$email' ");
            while($fila = $reg->fetch_assoc()){
                $id = $fila['id_usuario'];
            }
            if($this->db->affected_rows>0){
                $_SESSION['tempcod'] = $cod;
                $_SESSION['tempid'] = $id;
                $_SESSION['tempmail'] = $email;
                $_SESSION['tempnombre'] = $nombre." ".$apellido;
                $mail = new PHPMailer(true);

                try {
                    //Server settings
                    $mail->SMTPDebug = SMTP::DEBUG_OFF;                      // Enable verbose debug output
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                    $mail->Username   = 'admisecretaria133@gmail.com';                     // SMTP username
                    $mail->Password   = 'secre133adminis';                               // SMTP password
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                    $mail->Port       = 587;                                    // TCP port to connect to

                    //Recipients
                    $mail->setFrom('admisecretaria133@gmail.com', 'Secretaría Administrativa');
                    $mail->addAddress($email, $nombre." ".$apellido);     // Add a recipient
                   

                    // Content
                    $mail->Subject = 'Sistema Secretaría Administrativa - Correo de verificación de cuenta';
                    $mail->Body    = '<div align="center"><img scr="Views/Layouts/img/FISC.png" style="width:200px; height:200px:"></div><br><br><a href="http://localhost:8000?controller=Usuarios&action=statusUp&id='.$id.'&cod='.$cod.'">Clic aquí para activar tu cuenta</a>';
                    $mail->CharSet = 'UTF-8';
                    $mail->isHTML(true);                                  // Set email format to HTML

                    $mail->send();
                } catch (Exception $e) {
                    echo '<script> Command: toastr["error"]("Hubo un error al enviar el código al correo.", "ERROR") </script>';
                }
            }
        }
    }

    public function check_status(){
        if(isset($_GET['id']) && isset($_GET['cod'])){
            $id = $_GET['id'];
            $cod = $_GET['cod'];
            $query = $this->db->query("SELECT id_usuario, us_nombre, us_apellido, us_email, us_nacimiento, us_facultad, us_rol, us_status, us_codpase FROM Usuarios WHERE id_usuario = $id AND us_codpase = '$cod';");
            if(mysqli_num_rows($query)>0){
                $update = $this->db->query("UPDATE Usuarios SET us_status = 1 WHERE id_usuario = $id AND us_codpase = '$cod';");
                if($update){
                    while($fila = $query->fetch_assoc()){
                        $_SESSION['id'] = $fila["id_usuario"];
                        $_SESSION['nombre'] = $fila["us_nombre"];
                        $_SESSION['apellido'] = $fila["us_apellido"];
                        $_SESSION['email'] = $fila["us_email"];
                        $fecha = strtotime($fila["us_nacimiento"]);
                        $_SESSION['dia_nac'] = date("d", $fecha);
                        $_SESSION['mes_nac'] = date("m", $fecha);
                        $_SESSION['an_nac'] = date("Y", $fecha);
                        $_SESSION['facultad'] = $fila["us_facultad"];
                        $_SESSION['rol'] = $fila["us_rol"];
                    }
                    return 1;
                }
                else{
                    return 0;
                }
            }
            else{
                return 0;
            }
            
        }
        else{
            return 2;
        }
    }
}

?>