<?php
class ReparacionModel{
    private $db;
    private $solicitud;
 
    public function __construct(){
        $this->db = Conexion::conectar();
        $this->solicitud = array();
    }
    public function lista(){
        $consulta = $this->db->query("SELECT id_solicitud as id, sol_nombre as nombre, sol_lugardedao as lugar, sol_tipodao as tipo, sol_descripcion as descr, sol_fecha as fecha, sol_solicitante as solicitante, sol_estado as estado
             FROM Solicitud;");
         while($mostrar = mysqli_fetch_array($consulta)){
            $this->solicitud[] = $mostrar;
        }

        return $this->solicitud;
    }

    public function lista_mant(){
        $consulta = $this->db->query("SELECT id_solicitud as id, sol_nombre as nombre, sol_lugardedao as lugar, sol_tipodao as tipo, sol_descripcion as descr, sol_fecha as fecha, sol_solicitante as solicitante, sol_estado as estado
             FROM Solicitud
             WHERE sol_estado = 1;");
         while($mostrar = mysqli_fetch_array($consulta)){
            $this->solicitud[] = $mostrar;
        }

        return $this->solicitud;
    }
    public function actualizar_estado(){
       
        $id = array_values($_POST['id']);
        $estado = array_values($_POST['estado']);

        foreach($id as $index => $value){
            $regId = $id[$index];
            $regEst = $estado[$index];
            $this->db->query("UPDATE Solicitud SET sol_estado = $regEst WHERE id_solicitud = $regId");
        }
    }
    public function mi_lista(){
        $userid = $_SESSION['id'];

        $consulta = $this->db->query("SELECT id_solicitud as id, sol_nombre as nombre, sol_lugardedao as lugar, sol_tipodao as tipo, sol_descripcion as descr, sol_fecha as fecha, sol_solicitante as solicitante, sol_estado as estado
            FROM Solicitud WHERE sol_id_solicitante = $userid");
        
        while($mostrar = mysqli_fetch_array($consulta)){
            $this->solicitud[] = $mostrar;
        }
        return $this->solicitud;
    }
    public function guardar_solicitud(){
        $nombreSol = trim($_POST['nombreSolicitud']);
        $lugarDan = trim($_POST['lugarSolicitud']);
        $tipoDan = $_POST['tipoDaño'];
        $descrSol = $_POST['descr'];
        $fechaSol = date("Y-m-d");
        $solicitante = $_SESSION['nombre']." ".$_SESSION['apellido'];
        $idSolicitante = (int) $_SESSION['id'];
        $estadoSol = 0;

        if($_SESSION['rol'] == "Secretaria"){
            $estadoSol = 1;
        }

        $consulta = "INSERT INTO Solicitud (sol_nombre, sol_lugardedao, sol_tipodao, sol_descripcion, sol_fecha, sol_solicitante, sol_id_solicitante, sol_estado) 
            VALUES ('$nombreSol','$lugarDan','$tipoDan','$descrSol','$fechaSol','$solicitante',$idSolicitante,'$estadoSol')";
        
        $this->db->query($consulta); 
        
        if(!$this->db -> commit()){
            echo "commit fail";
            exit();
        }

        $this->db->close();
    }
    public function guardar_solicitudNoLog(){
        $nombre = trim($_POST['nombreSolicitante']);
        $apellido = trim($_POST['apellidoSolicitante']);
        $nombreSol = trim($_POST['nombreSolicitud']);
        $lugarDan = trim($_POST['lugarSolicitud']);
        $tipoDan = $_POST['tipoDaño'];
        $descrSol = $_POST['descr'];
        $fechaSol = date("Y-m-d");
        $solicitante = $nombre." ".$apellido;
        $estadoSol = 0;

        if($_SESSION['rol'] == "Secretaria"){
            $estadoSol = 1;
        }

        $consulta = "INSERT INTO Solicitud (sol_nombre, sol_lugardedao, sol_tipodao, sol_descripcion, sol_fecha, sol_solicitante, sol_estado) 
            VALUES ('$nombreSol','$lugarDan','$tipoDan','$descrSol','$fechaSol','$solicitante','$estadoSol')";

        $this->db->query($consulta);

        if(!$this->db -> commit()){
            echo "commit fail";
            exit();
        }

        $this->db->close();
    }
}

?>
