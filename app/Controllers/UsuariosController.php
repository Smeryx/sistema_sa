<?php 
/**
* 
*/
require_once('Models/UsuariosModel.php');

class UsuariosController
{
	
	function __construct()
	{
		
	}
	function perfil(){
		$usuario = new UsuariosModel();
		require_once('Views/Usuario/perfil.php');
	}
	function edit(){
		$usuario = new UsuariosModel();
		require_once('Views/Usuario/editperfil.php');
	}
	function update(){
		$usuario = new UsuariosModel();
		$usuario->update_info();
		echo "<script>location.href='?controller=Usuarios&action=perfil';</script>";
	}
	function signin(){
		$usuario = new UsuariosModel();
		require_once('Views/Usuario/signin.php');
	}
	function save(){
		$usuario = new UsuariosModel();
        $usuario->registrar();
        require_once('Views/Usuario/save.php');
	}
	function statusUp(){
		$usuario = new UsuariosModel();
		$status = $usuario->check_status();
		require_once('Views/Usuario/statusUp.php');
	}
	function logout(){
		session_unset();
		echo "<script>location.href='?controller=Home&action=index';</script>";
	}


}

?>