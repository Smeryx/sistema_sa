<?php 

require_once('Models/ReparacionModel.php');

class ReparacionController
{
    function __construct()
    {

    }
    function menu()
    {
        require_once('Views/Reparacion-Mantenimiento/menu.php');
    }
    function formulario_de_actualizacion()
    {
        require_once('Views/Reparacion-Mantenimiento/formulario_de_actualizacion.php');
    }
    function materiales()
    {
        require_once('Views/Reparacion-Mantenimiento/materiales.php');
    }
    function reporte()
    {
        require_once('Views/Reparacion-Mantenimiento/reporte.php');
    }
    function mostrarSolicitudes()
    {
        $solicitud = new ReparacionModel();
		$lista = $solicitud->lista();
        require_once('Views/Reparacion-Mantenimiento/mostrarSolicitudes.php');
    }
    function guardarSolicitudes()
    {
        $solicitud = new ReparacionModel();
		$solicitud->actualizar_estado();
        echo "<script> location.href='?controller=Reparacion&action=menu'; </script>";
    }
    function mantSolicitudes()
    {
        $solicitud = new ReparacionModel();
		$lista = $solicitud->lista_mant();
        require_once('Views/Reparacion-Mantenimiento/mantSolicitudes.php');
    }
    function nuevaSolicitud() 
    {
        require_once('Views/Reparacion-Mantenimiento/nuevaSolicitud.php');
    }
    function enviarSolicitud() 
    {   
        $solicitud = new ReparacionModel();
        $solicitud->guardar_solicitud();
        require_once('Views/Reparacion-Mantenimiento/solicitudEnviada.php');
    }
    function enviarSolicitudNoLog() 
    {   
        $solicitud = new ReparacionModel();
        $solicitud->guardar_solicitudNoLog();
        require_once('Views/Reparacion-Mantenimiento/solicitudEnviada.php');
    }
    function mySolicitudes() 
    {   
        $solicitud = new ReparacionModel();
        $milista = $solicitud->mi_lista();
        require_once('Views/Reparacion-Mantenimiento/mySolicitudes.php');
    }
}